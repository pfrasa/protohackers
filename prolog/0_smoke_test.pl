:- use_module(library(socket)).
:- use_module(library(readutil)).

create_server(Port) :-
  tcp_socket(SocketId),
  tcp_bind(SocketId, Port),
  tcp_listen(SocketId, 100),
  tcp_open_socket(SocketId, Socket, _),
  accept(Socket).

accept(Socket) :-
  tcp_accept(Socket, ClientSocket, _),
  thread_create(handle_client_with_setup(ClientSocket),_ , [ detached(true) ]),
  accept(Socket).

handle_client_with_setup(Socket) :-
  setup_call_cleanup(
    tcp_open_socket(Socket, StreamPair),
    handle_client(StreamPair),
    close(StreamPair)
  ).

handle_client(StreamPair) :-
  stream_pair(StreamPair, Read, Write),
  set_stream(Write, buffer(false)),
  get_code(Read, Input),
  handle_input(Input, Write, StreamPair).

handle_input(-1, _, _) :- !.
handle_input(Input, Write, StreamPair) :-
  put_code(Write, Input),
  handle_client(StreamPair).
