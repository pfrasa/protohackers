module VCS.Repository
  ( Repository
  , ListResponse
  , GetResponse
  , PutResponse
  , newRepository
  , list
  , get
  , put
  )
  where

import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text)

-- public

data Repository = Repository
                { latestRevision :: Revision
                , files :: FileStore
                , dirs :: DirListing
                }

newRepository :: Repository
newRepository = Repository defaultRevision emptyFileStore emptyDirListing

data ListResponse = ListResponse

list :: Repository -> Text -> Either Text ListResponse
list _ _ = Left "not implemented"

newtype GetResponse = GetResponse Text

get :: Repository -> Text -> Either Text GetResponse
get _ _ = Left "not implemented"

data PutResponse = PutResponse

put :: Repository -> Text -> Int -> Text -> Either Text (PutResponse, Repository)
put _ _ _ _ = Left "not implemented"

-- private 

defaultRevision :: Revision
defaultRevision = Revision "r1"

emptyFileStore :: FileStore
emptyFileStore = FileStore M.empty

emptyDirListing :: DirListing
emptyDirListing = DirListing M.empty

newtype Revision = Revision Text
newtype FileStore = FileStore (Map FullyQualifiedFileName TrackedFile)
newtype DirListing = DirListing (Map FullyQualifiedFileName DirContent)

newtype FullyQualifiedFileName = FullyQualifiedFileName [Text]
newtype RelativeFileName = RelativeFileName Text
data TrackedFile = TrackedFile 
                 { latestRevision :: Revision
                 , revisions:: FileRevisions
                 }
newtype FileRevisions = FileRevisions (Map Revision FileContent)
newtype FileContent = FileContent Text

newtype DirContent = DirContent [(RelativeFileName, FileType)]
data FileType = Dir | File