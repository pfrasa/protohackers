module LineReversal.LRCP
  ( accept
  , Session
  , SessionHandler
  , receive
  , send
  ) where

import Control.Concurrent
import Control.Monad (forM_, forever, when, void)
import Data.Bifunctor (second)
import Data.Functor ((<&>))
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IM
import Data.List.Split (chunksOf)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Network.Socket hiding (accept)
import Network.Socket.ByteString (sendTo)
import System.Timeout (timeout)
import LineReversal.Packet

accept :: HostAddress -> PortNumber -> SessionHandler -> IO ()
accept hostAddress port sessionHandler = do
  sock <- socket AF_INET Datagram defaultProtocol
  bind sock (SockAddrInet port hostAddress)

  queues <- Queues <$> newChan <*> newChan

  readThread <- forkIO $ readFromSocket sock queues
  writeThread <- forkIO $ writeToSocket sock queues

  handlePackets sessionHandler queues M.empty

  killThread readThread
  killThread writeThread

data Session = Session
  { _sockAddr :: SockAddr
  , _sessionId :: SessionId
  , _receiveQueue :: MessageQueue
  , _sessionQueue :: SessionQueue
  , _alive :: MVar Bool
  }

type SessionHandler = Session -> IO ()

receive :: Session -> IO Text
receive session = readChan (_receiveQueue session)

send :: Session -> Text -> IO ()
send session msg = writeChan (_sessionQueue session) (SendMessage msg)

type PacketsQueue = Chan Packet
type SendQueue = Chan (SockAddr, Text)
type MessageQueue = Chan Text
type SessionQueue = Chan SessionEvent

data SessionEvent = DataReceived Pos Text | AckReceived Length | SendMessage Text deriving (Eq, Show)

data Queues = Queues
  { _packetsQueue :: PacketsQueue
  , _sendQueue :: SendQueue
  }

mkSession :: SockAddr -> SessionId -> IO Session
mkSession sockAddr sessionId = Session sockAddr sessionId <$> newChan <*> newChan <*> newMVar True

type Sessions = HashMap SessionId ([ThreadId], Session)

readFromSocket :: Socket -> Queues -> IO ()
readFromSocket sock queues = forever $ do
  ePacket <- readPacket sock
  forM_ ePacket $ \packet -> writeChan (_packetsQueue queues) packet

writeToSocket :: Socket -> Queues -> IO ()
writeToSocket sock queues = forever $ do
  (sockAddr, msg) <- readChan (_sendQueue queues)
  _ <- sendTo sock (encodeUtf8 msg) sockAddr
  return ()

handlePackets :: SessionHandler -> Queues -> Sessions -> IO ()
handlePackets sessionHandler queues = loop
  where
    loop :: Sessions -> IO ()
    loop sessions = do
      packet <- readChan (_packetsQueue queues)

      newSessions <- case packet of
        Connect sessionId sockAddr -> do
          writeChan (_sendQueue queues) (sockAddr, mkAckPacket sessionId 0)

          if not (M.member sessionId sessions)
            then do
              newSession <- mkSession sockAddr sessionId
              state <- mkSessionState
              sessionThread <- forkIO $ processSessionQueue queues newSession state
              handlerThread <- forkIO $ sessionHandler newSession
              return $ M.insert sessionId ([sessionThread, handlerThread], newSession) sessions
            else return sessions

        Data sessionId sockAddr pos msg -> do
          case M.lookup sessionId sessions of
            Just (_, session) -> do
              writeChan (_sessionQueue session) (DataReceived pos msg)
            Nothing ->
              writeChan (_sendQueue queues) (sockAddr, mkClosePacket sessionId)

          return sessions

        Ack sessionId sockAddr len -> do
          case M.lookup sessionId sessions of
            Just (_, session) ->
              writeChan (_sessionQueue session) (AckReceived len)
            Nothing ->
              writeChan (_sendQueue queues) (sockAddr, mkClosePacket sessionId)

          return sessions

        Close sessionId sockAddr -> do
          writeChan (_sendQueue queues) (sockAddr, mkClosePacket sessionId)

          forM_ (M.lookup sessionId sessions) $ \(threadIds, session) -> do
            void $ swapMVar (_alive session) False
            traverse killThread threadIds

          return $ M.delete sessionId sessions

      loop newSessions

data SessionState = SessionState
  { _receivedData :: IntMap Char
  , _lastAckSent :: Length
  , _readStart :: Pos
  , _sentData :: [Char]
  , _lastAckReceived :: MVar Length
  }

mkSessionState :: IO SessionState
mkSessionState = SessionState IM.empty 0 0 [] <$> newMVar 0

processSessionQueue :: Queues -> Session -> SessionState -> IO ()
processSessionQueue queues session = loop
  where
    loop :: SessionState -> IO ()
    loop state = do
      event <- readChan (_sessionQueue session)

      case event of
        DataReceived pos msg -> do
          let newReceived = _receivedData state `IM.union` IM.fromList (zip [(fromIntegral pos)..] (T.unpack msg))

          if pos <= _lastAckSent state
            then do
              let newAck = fromIntegral $ contiguousLength newReceived
              writeChan (_sendQueue queues) (_sockAddr session, mkAckPacket (_sessionId session) newAck)

              let (newStart, messages) = lookForMessages (_readStart state) newReceived
              forM_ messages $ \message -> do
                writeChan (_receiveQueue session) message

              loop state { _receivedData = newReceived, _lastAckSent = newAck, _readStart = newStart }

            else do
              writeChan (_sendQueue queues) (_sockAddr session, mkAckPacket (_sessionId session) (_lastAckSent state))
              loop state { _receivedData = newReceived }

        AckReceived len -> do
          if fromIntegral len > length (_sentData state) then do
            writeChan (_packetsQueue queues) (Close (_sessionId session) (_sockAddr session))
          else do
            lastAck <- readMVar (_lastAckReceived state)
            when (len > lastAck) $ do
              void $ swapMVar (_lastAckReceived state) len
              let notReceived = drop (fromIntegral len) (_sentData state)
              forM_ (zip [0..] (chunksOf maxMsgSize notReceived)) $ \(i, chunk) -> do
                let pos = len + (fromIntegral maxMsgSize) * i
                let packet = mkDataPacket (_sessionId session) pos chunk
                writeChan (_sendQueue queues) (_sockAddr session, packet)
            loop state

        SendMessage msg -> do
          let terminated = T.unpack msg <> "\n"
          let packets = zip [0..] (chunksOf maxMsgSize terminated) <&> \(i, packet) ->
                          let pos = fromIntegral $ (length . _sentData) state + maxMsgSize * i
                          in  (pos + fromIntegral (length packet), mkDataPacket (_sessionId session) pos packet)
          forM_ packets $ \packet ->
            void $ forkIO $
              sendPacketWaitingForAck (_alive session) (_lastAckReceived state) (_sendQueue queues) (_sockAddr session) packet
          loop $ state { _sentData = _sentData state ++ terminated }

sendPacketWaitingForAck :: MVar Bool -> MVar Length -> SendQueue -> SockAddr -> (Length, Text) -> IO ()
sendPacketWaitingForAck sessionAlive lastAckReceived sendQueue sockAddr (ackRequested, packet) =
  void $ timeout (60 * 10^(6 :: Int)) loop
  where
    loop :: IO ()
    loop = do
      writeChan sendQueue (sockAddr, packet)
      threadDelay (3 * 10^(6 :: Int))
      lastAck <- readMVar lastAckReceived
      isAlive <- readMVar sessionAlive
      when (isAlive && lastAck < ackRequested) loop

contiguousLength :: IntMap a -> Int
contiguousLength imap = go 0
  where
    go :: Int -> Int
    go i =
      if IM.member i imap then
        go (i+1)
      else
        i

lookForMessages :: Pos -> IntMap Char -> (Pos, [Text])
lookForMessages startPos imap = reverse <$> go startPos []
  where
    go :: Pos -> [Text] -> (Pos, [Text])
    go pos texts =
      case lookForMessage pos imap of
        Nothing -> (pos, texts)
        Just (newPos, text) -> go newPos (text : texts)

lookForMessage :: Pos -> IntMap Char -> Maybe (Pos, Text)
lookForMessage startPos imap = second (T.pack . reverse) <$> go startPos ""
  where
    go :: Pos -> String -> Maybe (Pos, String)
    go i acc =
      case IM.lookup (fromIntegral i) imap of
        Nothing -> Nothing
        Just '\n' -> Just (i + 1, acc)
        Just c -> go (i + 1) (c : acc)

-- the encoded message + headers should be < 1000 bytes
-- as a heuristic, we just set the maximum unencoded message length,
-- so we don't have to encode it in order to determine if it's too long
maxMsgSize :: Int
maxMsgSize = 900