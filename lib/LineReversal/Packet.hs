module LineReversal.Packet
  ( SessionId
  , Pos
  , Length
  , Packet (..)
  , readPacket
  , mkAckPacket
  , mkClosePacket
  , mkDataPacket
  ) where

import Control.Arrow (left)
import Data.ByteString (ByteString)
import Data.Function ((&))
import Data.Text (Text)
import qualified Data.Text as T
import Data.Word (Word32)
import Network.Socket
import Network.Socket.ByteString
import Data.Text.Encoding (decodeUtf8')
import Text.Read (readEither)

type SessionId = Word32
type Pos = Word32
type Length = Word32

data Packet
  = Connect SessionId SockAddr
  | Data SessionId SockAddr Pos Text
  | Ack SessionId SockAddr Length
  | Close SessionId SockAddr
  deriving (Eq, Show)

maxPacketSize :: Int
maxPacketSize = 1000

readPacket :: Socket -> IO (Either Text Packet)
readPacket sock = do
  (bs, clientAddr) <- recvFrom sock maxPacketSize
  return $ parsePacket bs clientAddr

parsePacket :: ByteString -> SockAddr -> Either Text Packet
parsePacket bs sockAddr = do
  decoded <- left (T.pack . show) $ decodeUtf8' bs

  case splitPacket decoded of
    ["connect", sessionIdStr] -> do
      sessionId <- readInt sessionIdStr
      Right $ Connect sessionId sockAddr

    ["data", sessionIdStr, posStr, dataStr] -> do
      sessionId <- readInt sessionIdStr
      pos <- readInt posStr
      Right $ Data sessionId sockAddr pos dataStr

    ["ack", sessionIdStr, lengthStr] -> do
      sessionId <- readInt sessionIdStr
      len <- readInt lengthStr
      Right $ Ack sessionId sockAddr len

    ["close", sessionIdStr] -> do
      sessionId <- readInt sessionIdStr 
      Right $ Close sessionId sockAddr

    _ -> Left "invalid message"

splitPacket :: Text -> [Text]
splitPacket str
  | validBeginAndEnd =
      str
        -- we know that inputs are always ASCII
        -- so escaping becomes easier to handle if we temporarily use non-ASCII characters
        & T.replace "\\/" "ü" . T.replace "\\\\" "ö"
        & T.splitOn "/"
        & map (T.replace "ö" "\\" . T.replace "ü" "/")
        & filter (not . T.null)
  | otherwise = []
  where
    validBeginAndEnd :: Bool
    validBeginAndEnd = T.isPrefixOf "/" str && T.isSuffixOf "/" str

readInt :: Text -> Either Text Word32
readInt = left T.pack . readEither . T.unpack

mkAckPacket :: SessionId -> Length -> Text
mkAckPacket sessionId len = T.pack $ "/ack/" <> show sessionId <> "/" <> show len <> "/"

mkClosePacket :: SessionId -> Text
mkClosePacket sessionId = T.pack $ "/close/" <> show sessionId <> "/"

mkDataPacket :: SessionId -> Pos -> [Char] -> Text
mkDataPacket sessionId pos msg = T.pack $ "/data/" <> show sessionId <> "/" <> show pos <> "/" <> encodedMsg <> "/"
  where
    encodedMsg :: [Char]
    encodedMsg = msg & T.pack & T.replace "\\" "\\\\" & T.replace "/" "\\/" & T.unpack