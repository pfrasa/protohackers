{-# LANGUAGE TupleSections #-}

module JobCentre.JobServer
  ( JobServer(..)
  , HandleResult(..)
  , mkJobServer
  , mkClientId
  , handleRequest
  , addClient
  , removeClient
  , containsClient
  )
  where

import Control.Concurrent (MVar, newMVar, takeMVar, putMVar)
import Data.List (maximumBy, find, foldl')
import Data.Map (Map)
import qualified Data.Map as M
import Data.PQueue.Max (MaxQueue)
import qualified Data.PQueue.Max as MQ
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.UUID as UUID
import qualified Data.UUID.V4 as UUID4

import JobCentre.API
import JobCentre.Common

newtype JobServer = JobServer (MVar State)

data HandleResult = Direct Response | TryAgain | Empty deriving (Eq, Show)

mkJobServer :: IO JobServer
mkJobServer = JobServer <$> newMVar (State 1 M.empty M.empty Set.empty)

mkClientId :: IO ClientId
mkClientId = T.pack . UUID.toString <$> UUID4.nextRandom

handleRequest :: JobServer -> Request -> ClientId -> IO HandleResult
handleRequest jobServer request clientId =
  case request of
    Put queueId job priority -> withState jobServer $ \(State nextJobId assignments queues clients) ->
      let jobDef = JobDefinition queueId nextJobId priority job
          newQueue = case M.lookup queueId queues of
                       Nothing -> Queue queueId (MQ.singleton jobDef)
                       Just (Queue _ jobs) -> Queue queueId $ MQ.insert jobDef jobs
          newState = State (nextJobId + 1) assignments (M.insert queueId newQueue queues) clients
      in  (newState, Direct (OkPut nextJobId))

    Get queueIds wait -> handleWait wait jobServer $ \state@(State nextJobId assignments queues clients) -> do
      case findMaxPrioQueue queueIds (M.elems queues) of
        Nothing -> (state, Direct NoJob)
        Just (Queue queueId jobs) ->
          case MQ.maxView jobs of
            Nothing -> (state, Direct NoJob)
            Just (jd@(JobDefinition _ jid priority job), js) ->
              let newQueue = Queue queueId  js
                  newState = State nextJobId (M.insert jid (jd, clientId) assignments) (M.insert queueId newQueue queues) clients
              in  (newState, Direct (OkGet queueId jid job priority))

    Delete jobId -> withState jobServer $ \state@(State nextJobId assignments queues clients) ->
      case findQueueWithJob (M.elems queues) jobId of
        Nothing ->
          if M.member jobId assignments then
            (State nextJobId (M.delete jobId assignments) queues clients, Direct Ok)
          else
            (state, Direct NoJob)
        Just (Queue qid jobs) ->
          let newQueue = Queue qid (MQ.filter (\(JobDefinition _ jid _ _) -> jid /= jobId) jobs)
              newState = State nextJobId (M.delete jobId assignments) (M.insert qid newQueue queues) clients
          in  (newState, Direct Ok)

    Abort jobId -> withState jobServer $ \state@(State _ assignments _ _) ->
      case M.lookup jobId assignments of
        Nothing -> (state, Direct NoJob)
        Just (jd, cid) ->
          if cid /= clientId then
            (state, Direct ErrorResponse)
          else
            (abortJob state jd, Direct Ok)

    Disconnect -> withState jobServer $ \state@(State _ assignments _ _) ->
      let jobsToAbort = map fst $ filter (\(_, cid) -> cid == clientId) $ M.elems assignments
          newState = foldl' abortJob state jobsToAbort
      in  (removeClient clientId newState, Empty)

    Invalid -> withState jobServer (, Direct ErrorResponse)

  where
    findMaxPrioQueue :: [QueueId] -> [Queue] -> Maybe Queue
    findMaxPrioQueue _ [] = Nothing
    findMaxPrioQueue ids queues = Just $ maximumBy isHigherPrioQueue . filter (\(Queue qid _) -> qid `elem` ids) $ queues

    isHigherPrioQueue :: Queue -> Queue -> Ordering
    isHigherPrioQueue (Queue _ jobs1) (Queue _ jobs2) =
      case (MQ.getMax jobs1, MQ.getMax jobs2) of
        (_, Nothing) -> GT
        (Nothing, Just _) -> LT
        (Just j1, Just j2) -> compare j1 j2

    findQueueWithJob :: [Queue] -> JobId -> Maybe Queue
    findQueueWithJob queues jobId =
        find (\(Queue _ jobs) -> (not . MQ.null) (MQ.filter (\(JobDefinition _ jid _ _) -> jid == jobId) jobs)) queues

    abortJob :: State -> JobDefinition -> State
    abortJob (State nextJobId assignments queues clients) jd@(JobDefinition qid jobId _ _) =
      let newQueue = case M.lookup qid queues of
                          Nothing -> Queue qid (MQ.singleton jd)
                          Just (Queue _ jobs) -> Queue qid $ MQ.insert jd jobs
      in  State nextJobId (M.delete jobId assignments) (M.insert qid newQueue queues) clients

data State = State JobId (Map JobId (JobDefinition, ClientId)) (Map QueueId Queue) (Set ClientId)
data Queue = Queue QueueId (MaxQueue JobDefinition)
data JobDefinition = JobDefinition QueueId JobId Priority Job deriving (Eq, Show)

instance Ord JobDefinition where
  JobDefinition _ _ p1 _ <= JobDefinition _ _ p2 _ = p1 <= p2

addClient :: ClientId -> State -> State
addClient clientId (State nextJobId assignments queues clients) =
  State nextJobId assignments queues (Set.insert clientId clients)

removeClient :: ClientId -> State -> State
removeClient clientId (State nextJobId assignments queues clients) =
  State nextJobId assignments queues (Set.filter (/= clientId) clients)

containsClient :: State -> ClientId -> Bool
containsClient (State _ _ _ clients) clientId = clientId `elem` clients

withState :: JobServer -> (State -> (State, b)) -> IO b
withState (JobServer mvar) f = do
  state <- takeMVar mvar
  let (updatedState, result) = f state
  putMVar mvar updatedState
  return result

handleWait :: Bool -> JobServer -> (State -> (State, HandleResult)) -> IO HandleResult
handleWait wait js f = do
  result <- withState js f

  if result == Direct NoJob && wait then
    return TryAgain
  else
    return result