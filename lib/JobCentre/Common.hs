module JobCentre.Common
  ( QueueId
  , Job
  , Priority
  , Wait
  , JobId
  , ClientId
  ) where

import Data.Text (Text)
import Data.Aeson (Object)

type QueueId = Text
type Job = Object
type Priority = Int
type Wait = Bool
type JobId = Int
type ClientId = Text
