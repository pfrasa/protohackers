module JobCentre.API
  ( Request(..)
  , decodeRequest
  , Response(..)
  , encodeResponse
  )
  where

import Data.Aeson
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as BSL

import JobCentre.Common

data Request
  = Put QueueId Job Priority
  | Get [QueueId] Wait
  | Delete JobId
  | Abort JobId
  | Disconnect
  | Invalid
  deriving (Eq, Show)

instance FromJSON Request where
  parseJSON = withObject "Request" $ \obj -> do
    requestType :: Text <- obj .: "request"
    case requestType of
      "put" -> Put <$> obj .: "queue" <*> obj .: "job" <*> obj .: "pri"
      "get" -> Get <$> obj .: "queues" <*> (fromMaybe False <$> obj .:? "wait")
      "delete" -> Delete <$> obj .: "id"
      "abort" -> Abort <$> obj .: "id"
      _ -> fail "Bad Request"

decodeRequest :: ByteString -> Request
decodeRequest = fromMaybe Invalid . decode . BSL.fromStrict

data Response
  = OkPut JobId
  | OkGet QueueId JobId Job Priority 
  | Ok
  | NoJob
  | ErrorResponse
  deriving (Eq, Show)

instance ToJSON Response where
  toJSON (OkPut jobId) = object ["status" .= ok, "id" .= jobId]
  toJSON (OkGet queue jobId job priority) =
    object [ "status" .= ok
           , "queue" .= queue
           , "id" .= jobId
           , "job" .= job
           , "pri" .= priority
           ]
  toJSON Ok = object ["status" .= ok]
  toJSON NoJob = object ["status" .= noJob]
  toJSON ErrorResponse = object ["status" .= errorT]

encodeResponse :: Response -> ByteString
encodeResponse = (<> "\n") . BSL.toStrict . encode

ok :: Text
ok = "ok"

noJob :: Text
noJob = "no-job"

errorT :: Text
errorT = "error"