module JobCentre.Network
  ( recvRequest
  , sendResponse
  )
  where

import Network.Simple.TCP (Socket, recv, send)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS

import JobCentre.API

recvRequest :: Socket -> IO Request
recvRequest sock = maybe Disconnect decodeRequest <$> recvLine sock

sendResponse :: Socket -> Response -> IO ()
sendResponse sock response = send sock $ encodeResponse response

recvLine :: Socket -> IO (Maybe ByteString)
recvLine sock = fmap BS.reverse <$> go ""
  where
    go :: ByteString -> IO (Maybe ByteString)
    go acc = do
      mBytes <- recv sock 1
      case mBytes of
        Nothing -> return Nothing
        Just bytes ->
          if bytes == "\n" then
            return (Just acc)
          else
            go (bytes <> acc)