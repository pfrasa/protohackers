module SpeedDaemon.ClientHandler
  ( ClientState
  , mkClientState
  , handleClient
  ) where

import Control.Concurrent (ThreadId, killThread, newMVar, takeMVar)
import Data.Foldable (forM_)
import Data.Text (Text)
import qualified Data.Text as T
import Network.Simple.TCP (Socket)
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Core.Queue
import SpeedDaemon.Core.Queues (Queues(..), TicketEvent (..), Measurement(..))
import SpeedDaemon.Network.ClientGateway (receiveMessage)
import SpeedDaemon.Actor.Heartbeat (processHeartbeats)

data ClientType
  = Unknown
  | Camera Road Mile Limit
  | Dispatcher
  deriving (Eq, Show)

data ClientState = ClientState
  { _queues :: Queues
  , _socket :: Socket
  , _type :: ClientType
  , _hbThread :: Maybe ThreadId
  }

mkClientState :: Queues -> Socket -> ClientState
mkClientState queues sock = ClientState queues sock Unknown Nothing

handleClient :: ClientState -> IO ()
handleClient state = do
  message <- receiveMessage (_socket state)

  case message of
    Left err -> disconnectWith err
    Right msg -> do
      eUpdated <- handleMessage state msg
      case eUpdated of
        Left err -> disconnectWith err
        Right updated -> handleClient updated

  where
    disconnectWith :: Text -> IO ()
    disconnectWith err = do
      killHeartbeat
      -- wait for acknowledgement before returning from the thread (and therefore closing the socket)
      enqueueAndWaitForAck (Error err)

    enqueueAndWaitForAck :: ServerMessage -> IO ()
    enqueueAndWaitForAck msg = do
      ack <- newMVar ()
      _ <- takeMVar ack
      enqueue ((_send . _queues) state) (_socket state, msg, Just ack)
      _ <- takeMVar ack
      return ()


    killHeartbeat :: IO ()
    killHeartbeat =  forM_ (_hbThread state) $ \threadId -> killThread threadId

handleMessage :: ClientState -> ClientMessage -> IO (Either Text ClientState)
handleMessage state msg =
  case msg of

    WantHeartbeat interval ->
      case _hbThread state of
        Nothing -> do
          if interval == 0 then
            return $ Right state
          else do
            threadId <- processHeartbeats (_socket state) interval ((_send . _queues) state)
            return $ Right $ state { _hbThread = Just threadId }
        Just _ -> return $ Left "Heartbeat already requested"

    IAmCamera road mile limit ->
      if _type state == Unknown then
        return $ Right $ state { _type = Camera road mile limit }
      else
        return $ Left $ T.pack $ "Already identified as " <> show (_type state)

    IAmDispatcher roads ->
      if _type state == Unknown then do
        enqueue ((_tickets . _queues) state) $ RegisterDispatcher (_socket state) roads
        return $ Right $ state { _type = Dispatcher }
      else
        return $ Left $ T.pack $ "Already identified as " <> show (_type state)

    Plate plateId timestamp ->
      case _type state of
        Camera road mile limit -> do
          enqueue ((_measurements . _queues) state) $ Measurement plateId road mile limit timestamp
          return $ Right state

        _ -> return $ Left "Only valid for clients identified as camera"
