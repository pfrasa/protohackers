module SpeedDaemon.Actor.MeasurementHandler
  ( startMeasurementHandling
  ) where

import Control.Concurrent (ThreadId, forkIO)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import Data.List (find)
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import SpeedDaemon.Core.BaseTypes hiding (_road)
import SpeedDaemon.Core.Queue
import SpeedDaemon.Core.Queues

startMeasurementHandling :: Queues -> IO ThreadId
startMeasurementHandling queues = forkIO $ handle M.empty
  where
    handle :: CarHandlers -> IO ()
    handle handlers = do
      measurement <- dequeue (_measurements queues)
      let car = _plateId measurement

      case M.lookup car handlers of
        Just queue -> do
          enqueue queue measurement
          handle handlers
        Nothing -> do
          newQueue <- mkQueue
          enqueue newQueue measurement
          _ <- forkIO $ handleMeasurementsPerCar queues newQueue mkState
          handle $ M.insert car newQueue handlers

handleMeasurementsPerCar :: Queues -> MeasurementQueue -> State -> IO ()
handleMeasurementsPerCar queues queue = loop
  where
    loop :: State -> IO ()
    loop state = do
      measurement <- dequeue queue

      let oldMeasurements = _measurementsPerRoad state
      let otherMeasurements = fromMaybe [] $ M.lookup (_road measurement) oldMeasurements
      let newMeasurements = addMeasurement oldMeasurements measurement

      let daysFined = _daysFined state
      newDaysFined <- case maybeWriteTicket daysFined measurement otherMeasurements of
        Nothing -> return daysFined
        Just ticket -> do
          enqueue (_tickets queues) $ SendTicket ticket
          return $ addFine daysFined (_timestamp1 ticket) (_timestamp2 ticket)

      loop $ State newDaysFined newMeasurements

    maybeWriteTicket :: Set Timestamp -> Measurement -> [Measurement] -> Maybe Ticket
    maybeWriteTicket daysFined measurement otherMeasurements = do
      prevMeasurement <- find (shouldWriteTicket daysFined measurement) otherMeasurements

      let (measurement1, measurement2) = if _timestamp prevMeasurement < _timestamp measurement
                                           then (prevMeasurement, measurement)
                                           else (measurement, prevMeasurement)

      let speed = calculateSpeed measurement1 measurement2

      return $ Ticket (_plateId measurement)
                      (_road measurement)
                      (_mile measurement1)
                      (_timestamp measurement1)
                      (_mile measurement2)
                      (_timestamp measurement2)
                      (round (100 * speed))

    shouldWriteTicket :: Set Timestamp -> Measurement -> Measurement -> Bool
    shouldWriteTicket daysFined measurement reference =
      matchesForSpeeding measurement reference &&
        not (alreadyFinedToday daysFined (_timestamp measurement) (_timestamp reference))

    matchesForSpeeding :: Measurement -> Measurement -> Bool
    matchesForSpeeding measurement reference =
      calculateSpeed measurement reference > fromIntegral (_limit measurement)

     -- in miles per hour
    calculateSpeed :: Measurement -> Measurement -> Double
    calculateSpeed measurement1 measurement2 =
      calculate' ((fromIntegral . _mile) measurement1, (fromIntegral . _timestamp) measurement1)
                 ((fromIntegral . _mile) measurement2, (fromIntegral . _timestamp) measurement2)
      where
        calculate' :: (Double, Double) -> (Double, Double) -> Double
        calculate' (mile1', timestamp1') (mile2', timestamp2') =
          let mps = abs ((mile2' - mile1') / (timestamp2' - timestamp1'))
          in  3600 * mps

type CarHandlers = HashMap PlateId MeasurementQueue

data State = State
  { _daysFined :: Set Timestamp
  , _measurementsPerRoad :: HashMap Road [Measurement]
  } deriving (Eq, Show)

addMeasurement :: HashMap Road [Measurement] -> Measurement -> HashMap Road [Measurement]
addMeasurement perRoad measurement =
  let measurements = fromMaybe [] $ M.lookup (_road measurement) perRoad
  in  M.insert (_road measurement) (measurement : measurements) perRoad

mkState :: State
mkState = State Set.empty M.empty

dayOfTimestamp :: Timestamp -> Timestamp
dayOfTimestamp t = t `div` 86400

alreadyFinedToday :: Set Timestamp -> Timestamp -> Timestamp -> Bool
alreadyFinedToday daysFined timestamp1 timestamp2 =
  Set.member (dayOfTimestamp timestamp1) daysFined ||
  Set.member (dayOfTimestamp timestamp2) daysFined

addFine :: Set Timestamp -> Timestamp -> Timestamp -> Set Timestamp
addFine daysFined timestamp1 timestamp2 =
  Set.insert (dayOfTimestamp timestamp1) $
    Set.insert (dayOfTimestamp timestamp2) daysFined