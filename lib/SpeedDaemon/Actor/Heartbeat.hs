{-# LANGUAGE NumericUnderscores #-}

module SpeedDaemon.Actor.Heartbeat
  ( processHeartbeats
  ) where

import Control.Concurrent (forkIO, threadDelay, ThreadId)
import Network.Simple.TCP (Socket)
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Core.Queue
import SpeedDaemon.Core.Queues (SendQueue)

data Heartbeat = Heartbeat
  { _clientSock :: Socket
  , _interval :: Deciseconds
  , _elapsed :: Deciseconds
  }

mkHeartbeat :: Socket -> Deciseconds -> Heartbeat
mkHeartbeat sock interval = Heartbeat sock interval 0

processHeartbeats :: Socket -> Deciseconds -> SendQueue -> IO ThreadId
processHeartbeats sock interval queue = forkIO $ process (mkHeartbeat sock interval)
  where
    process :: Heartbeat -> IO ()
    process heartbeat = do
      threadDelay 100_000
      updated <- processOnce
      process updated

      where
        processOnce :: IO Heartbeat
        processOnce =
          if _interval heartbeat == _elapsed heartbeat then do
            enqueue queue (_clientSock heartbeat, HeartbeatMsg, Nothing)
            return $ heartbeat { _elapsed = 0 }
        else return $ heartbeat { _elapsed = _elapsed heartbeat + 1 }