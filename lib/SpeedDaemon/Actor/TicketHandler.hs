module SpeedDaemon.Actor.TicketHandler
  ( startTicketHandling
  ) where

import Control.Concurrent (ThreadId, forkIO)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import Network.Simple.TCP (Socket)
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Core.Queue
import SpeedDaemon.Core.Queues hiding (_road)

startTicketHandling :: Queues -> IO ThreadId
startTicketHandling queues = forkIO $ handle mkDispatchers
  where
    handle :: Dispatchers -> IO ()
    handle dispatchers = do
      event <- dequeue (_tickets queues)

      updated <- case event of

        RegisterDispatcher sock roads ->
          return $ dispatchers `M.union` M.fromList [(road, sock) | road <- roads]

        SendTicket ticket -> do
          case M.lookup (_road ticket) dispatchers of
            Just sock -> enqueue (_send queues) (sock, TicketMsg ticket, Nothing)
            Nothing -> enqueue (_tickets queues) event

          return dispatchers
      
      handle updated

type Dispatchers = HashMap Road Socket

mkDispatchers :: Dispatchers
mkDispatchers = M.empty