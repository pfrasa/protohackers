module SpeedDaemon.Actor.MessageDispatcher
  ( startMessageDispatch
  ) where

import Control.Monad (forever)
import Control.Concurrent (ThreadId, forkIO, putMVar)
import SpeedDaemon.Core.Queue
import SpeedDaemon.Core.Queues (Queues(_send))
import SpeedDaemon.Network.ClientGateway (sendMessage)

startMessageDispatch :: Queues -> IO ThreadId
startMessageDispatch queues = forkIO dispatch
  where
    dispatch :: IO ()
    dispatch = forever $ do
      (sock, msg, mAck) <- dequeue (_send queues)
      sendMessage sock msg
      case mAck of
        Just ack -> putMVar ack ()
        Nothing -> return ()