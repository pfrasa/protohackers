module SpeedDaemon.Network.ClientGateway
  ( receiveMessage
  , sendMessage
  ) where

import Control.Exception (SomeException, catch)
import Data.Binary.Get (runGetIncremental, Decoder(..))
import Data.Binary.Put (runPut)
import qualified Data.ByteString.Lazy as LBS
import Data.Text (Text)
import qualified Data.Text as T
import Network.Simple.TCP (Socket, recv, send)
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Network.Encoding

receiveMessage :: Socket -> IO (Either Text ClientMessage)
receiveMessage sock = go (runGetIncremental decodeMessage)
  where
    go :: Decoder ClientMessage -> IO (Either Text ClientMessage)
    go (Fail _ _ err) = return $ Left (T.pack err)
    go (Done _ _ msg) = return $ Right msg
    go (Partial consumer) = do
      chunk <- recv sock 1
      go (consumer chunk)

sendMessage :: Socket -> ServerMessage -> IO ()
sendMessage sock msg = do
  let encoded = LBS.toStrict $ runPut $ encodeMessage msg
  -- ignore exceptions when the socket is closed
  send sock encoded `catch` \(_ :: SomeException) -> return ()
