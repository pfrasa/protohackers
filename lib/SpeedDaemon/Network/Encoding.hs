module SpeedDaemon.Network.Encoding
  ( decodeMessage
  , encodeMessage
  ) where

import Control.Monad (replicateM)
import Data.Binary.Get
import Data.Binary.Put
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeLatin1, encodeUtf8)
import SpeedDaemon.Core.BaseTypes

decodeMessage :: Get ClientMessage
decodeMessage = do
  msgType <- getWord8
  case msgType of
    0x20 -> Plate <$> decodeString <*> getWord32be
    0x40 -> WantHeartbeat <$> getWord32be
    0x80 -> IAmCamera <$> getWord16be <*> getWord16be <*> getWord16be
    0x81 -> do
      numRoads <- getWord8
      roads <- replicateM (fromIntegral numRoads) getWord16be
      return $ IAmDispatcher roads
    _ -> fail "Invalid client message"

decodeString :: Get Text
decodeString = do
  len <- getWord8
  decodeLatin1 <$> getByteString (fromIntegral len)

encodeMessage :: ServerMessage -> Put
encodeMessage (Error msg) = do
  putWord8 0x10
  encodeString msg
encodeMessage HeartbeatMsg = putWord8 0x41
encodeMessage (TicketMsg (Ticket plate road mile1 timestamp1 mile2 timestamp2 speed)) = do
  putWord8 0x21
  encodeString plate
  putWord16be road
  putWord16be mile1
  putWord32be timestamp1
  putWord16be mile2
  putWord32be timestamp2
  putWord16be speed

encodeString :: Text -> Put
encodeString s = do
  putWord8 $ fromIntegral (T.length s)
  putByteString $ encodeUtf8 s