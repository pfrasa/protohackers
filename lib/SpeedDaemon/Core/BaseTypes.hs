module SpeedDaemon.Core.BaseTypes
  ( PlateId
  , Timestamp
  , Deciseconds
  , Road
  , Mile
  , Limit
  , Speed
  , Ticket(..)
  , ClientMessage(..)
  , ServerMessage(..)
  ) where

import Data.Text (Text)
import Data.Word (Word16, Word32)

type PlateId = Text
type Timestamp = Word32
type Deciseconds = Word32
type Road = Word16
type Mile = Word16
type Limit = Word16
type Speed = Word16

data Ticket = Ticket
  { _plate :: PlateId
  , _road :: Road
  , _mile1 :: Mile
  , _timestamp1 :: Timestamp
  , _mile2 :: Mile
  , _timestamp2 :: Timestamp
  , _speed :: Speed
  }
  deriving (Eq, Show)

data ClientMessage
  = Plate PlateId Timestamp
  | WantHeartbeat Deciseconds
  | IAmCamera Road Mile Limit
  | IAmDispatcher [Road]
  deriving (Eq, Show)

data ServerMessage
  = Error Text
  | HeartbeatMsg
  | TicketMsg Ticket
  deriving (Eq, Show)