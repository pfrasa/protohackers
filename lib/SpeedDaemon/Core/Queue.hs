module SpeedDaemon.Core.Queue
  ( Queue
  , mkQueue
  , enqueue
  , dequeue
  ) where

import Control.Concurrent (Chan, newChan, writeChan, readChan)

newtype Queue a = Queue (Chan a)

mkQueue :: IO (Queue a)
mkQueue = Queue <$> newChan

enqueue :: Queue a -> a -> IO ()
enqueue (Queue chan) = writeChan chan

dequeue :: Queue a -> IO a
dequeue (Queue chan) = readChan chan