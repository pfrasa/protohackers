module SpeedDaemon.Core.Queues
  ( Queues(..)
  , mkQueues
  , SendQueue
  , TicketEventQueue
  , MeasurementQueue
  , TicketEvent(..)
  , Measurement(..)
  )
  where

import Control.Concurrent (MVar)
import Network.Simple.TCP (Socket)
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Core.Queue

data Queues = Queues
  { _send :: SendQueue
  , _tickets :: TicketEventQueue
  , _measurements :: MeasurementQueue
  }

mkQueues :: IO Queues
mkQueues = Queues <$> mkQueue <*> mkQueue <*> mkQueue

-- The MVar is used for an (optional) acknowledgement:
-- The consumer should fill the MVar after having sent the message
type SendQueue = Queue (Socket, ServerMessage, Maybe (MVar ()))

type TicketEventQueue = Queue TicketEvent

type MeasurementQueue = Queue Measurement

data TicketEvent
  = RegisterDispatcher Socket [Road]
  | SendTicket Ticket

data Measurement = Measurement
  { _plateId :: PlateId
  , _road :: Road
  , _mile :: Mile
  , _limit :: Limit
  , _timestamp :: Timestamp
  } deriving (Eq, Show)