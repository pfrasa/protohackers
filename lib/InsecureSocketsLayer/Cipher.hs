module InsecureSocketsLayer.Cipher
  ( CipherSpec(..)
  , CipherOp(..)
  , decodeCipherSpec
  , encryptBytes
  , decryptBytes
  , isInvalidCipher
  )
  where

import Data.Binary.Get
import Data.Bits (xor)
import Data.List (foldl')
import Data.Word (Word8, bitReverse8)
import Prelude hiding (zipWith)
import InsecureSocketsLayer.Zippable (Zippable, zipWith, fromList)

newtype CipherSpec = CipherSpec [CipherOp] deriving (Eq, Show)

data CipherOp
  = ReverseBits
  | Xor Int
  | XorPos
  | Add Int
  | AddPos
  deriving (Eq, Show)

decodeCipherSpec :: Get CipherSpec
decodeCipherSpec = CipherSpec <$> decode'
  where
    decode' :: Get [CipherOp]
    decode' = do
      op <- getWord8
      case op of
        0x00 -> return []
        0x01 -> (ReverseBits :) <$> decode'
        0x02 -> do
          n <- getWord8
          (Xor (fromIntegral n) :) <$> decode'
        0x03 -> (XorPos :) <$> decode'
        0x04 -> do
          n <- getWord8
          (Add (fromIntegral n) :) <$> decode'
        0x05 -> (AddPos :) <$> decode'
        _ -> fail "Invalid operation"

encryptBytes :: Zippable t => CipherSpec -> Int -> t Word8 -> t Word8
encryptBytes (CipherSpec ops) startPos = zipWith (\pos byte -> encryptByte pos byte ops) (fromList [startPos..])
  where
    encryptByte :: Int -> Word8 -> [CipherOp] -> Word8
    encryptByte pos = foldl' (encryptByteWith pos)

encryptByteWith :: Int -> Word8 -> CipherOp -> Word8
encryptByteWith pos byte op =
  case op of
    ReverseBits -> bitReverse8 byte
    Xor n -> byte `xor` fromIntegral n
    XorPos -> byte `xor` fromIntegral pos
    Add n -> byte + fromIntegral n
    AddPos -> byte + fromIntegral pos

decryptBytes :: Zippable t => CipherSpec -> Int -> t Word8 -> t Word8
decryptBytes (CipherSpec ops) startPos = zipWith (\pos byte -> decryptByte pos byte (reverse ops)) (fromList [startPos..])
  where
    decryptByte :: Int -> Word8 -> [CipherOp] -> Word8
    decryptByte pos = foldl' (decryptByteWith pos)

decryptByteWith :: Int -> Word8 -> CipherOp -> Word8
decryptByteWith pos byte op = 
  case op of
    ReverseBits -> bitReverse8 byte
    Xor n -> byte `xor` fromIntegral n
    XorPos -> byte `xor` fromIntegral pos
    Add n -> byte - fromIntegral n
    AddPos -> byte - fromIntegral pos

isInvalidCipher :: CipherSpec -> Bool
isInvalidCipher cipherSpec = encryptBytes cipherSpec 0 [0..255] == [0..255]