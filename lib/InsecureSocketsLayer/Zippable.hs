module InsecureSocketsLayer.Zippable (Zippable(..), ZStream(..)) where

import Data.Coerce (coerce)
import Prelude hiding (zip, zipWith)
import qualified Prelude as Pre
import Streaming
import qualified Streaming.Prelude as S

class Zippable t where
  fromList :: [a] -> t a
  zip :: t a -> t b -> t (a, b)
  zipWith :: (a -> b -> c) -> t a -> t b -> t c

instance Zippable [] where
  fromList = id
  zip = Pre.zip
  zipWith = Pre.zipWith

newtype ZStream a = ZStream (Stream (Of a) IO ())

instance Zippable ZStream where
  fromList = ZStream . S.each
  zip = coerce S.zip
  zipWith = coerce S.zipWith