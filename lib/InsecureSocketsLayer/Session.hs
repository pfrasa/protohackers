module InsecureSocketsLayer.Session
  ( handleClient
  , decryptedLines
  , findToyToCopy
  )
  where

import Data.Binary.Get (Decoder(..), Get, runGetIncremental)
import qualified Data.ByteString as BS
import Data.Char (isDigit)
import Data.Coerce (coerce)
import Data.Foldable (maximumBy)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Word (Word8)
import Network.Simple.TCP (Socket, recv, send, closeSock)
import Streaming
import qualified Streaming.Prelude as S
import InsecureSocketsLayer.Cipher
import InsecureSocketsLayer.Zippable

handleClient :: Socket -> IO ()
handleClient sock = do
  eCipherSpec <- recvDecoded decodeCipherSpec sock
  case eCipherSpec of
    Left _ -> closeSock sock
    Right cipherSpec ->
      if isInvalidCipher cipherSpec then
        closeSock sock
      else
        loop cipherSpec 0 (decryptedLines cipherSpec sock)
  where
    loop :: CipherSpec -> SentData -> Stream (Of Text) IO () -> IO ()
    loop cipherSpec sentData lineStream = do
      readStream <- S.uncons lineStream
      case readStream of
        Nothing -> return ()
        Just (nextLine, remaining) -> do
          let response = findToyToCopy nextLine <> "\n"
          let encoded = BS.unpack $ encodeUtf8 response
          let encrypted = encryptBytes cipherSpec sentData encoded
          send sock (BS.pack encrypted)
          loop cipherSpec (sentData + length encrypted) remaining

type SentData = Int

recvDecoded :: Get a -> Socket -> IO (Either Text a)
recvDecoded get sock = go (runGetIncremental get)
  where
    go :: Decoder a -> IO (Either Text a)
    go (Fail _ _ err) = return $ Left (T.pack err)
    go (Done _ _ msg) = return $ Right msg
    go (Partial consumer) = do
      chunk <- recv sock 1
      go (consumer chunk)

decryptedLines :: CipherSpec -> Socket -> Stream (Of Text) IO ()
decryptedLines cipherSpec sock = S.filter (not . T.null) $ (decodeUtf8 . BS.pack) `S.map` decryptedByteLines
  where
    decryptedByteLines :: Stream (Of [Word8]) IO ()
    decryptedByteLines = S.toList `S.mapped` S.split 10 decryptedByteStream

    decryptedByteStream :: Stream (Of Word8) IO ()
    decryptedByteStream = coerce $ decryptBytes cipherSpec 0 (ZStream byteStream)

    byteStream :: Stream (Of Word8) IO ()
    byteStream = S.concat $ BS.unpack `S.map` S.concat (S.takeWhile (/= Nothing) (S.repeatM (recv sock 1024)))

findToyToCopy :: Text -> Text
findToyToCopy line = snd $ maximumBy (\x y -> compare (fst x) (fst y)) $ map parse $ T.split (== ',') line
  where
    parse :: Text -> (Int, Text)
    parse t = (getAmount t, t)

    getAmount :: Text -> Int
    getAmount = read . T.unpack . T.takeWhile isDigit