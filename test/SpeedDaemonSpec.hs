module SpeedDaemonSpec where

import Test.Hspec
import Control.Concurrent (forkIO)
import Control.Monad (forM_)
import Data.Binary.Get (runGet)
import Data.Binary.Put (runPut)
import qualified Data.ByteString.Lazy as LBS
import Network.Simple.TCP
import Network.Socket
import SpeedDaemon.Core.BaseTypes
import SpeedDaemon.Network.Encoding
import SpeedDaemon.Network.ClientGateway

spec :: Spec
spec = do
  describe "SpeedDaemon.Network.Encoding" $ do
    describe "decodeMessage" $ do
      let expectations = [ ( [0x20, 0x04, 0x55, 0x4e, 0x31, 0x58, 0x00, 0x00, 0x03, 0xe8]
                          , Plate "UN1X" 1000
                          )
                        , ( [0x20, 0x07, 0x52, 0x45, 0x30, 0x35, 0x42, 0x4b, 0x47, 0x00, 0x01, 0xe2, 0x40]
                          , Plate "RE05BKG" 123456
                          )
                        , ( [0x40, 0x00, 0x00, 0x00, 0x0a]
                          , WantHeartbeat 10
                          )
                        , ( [0x40, 0x00, 0x00, 0x04, 0xdb]
                          , WantHeartbeat 1243
                          )
                        , ( [0x80, 0x00, 0x42, 0x00, 0x64, 0x00, 0x3c]
                          , IAmCamera 66 100 60
                          )
                        , ( [0x80, 0x01, 0x70, 0x04, 0xd2, 0x00, 0x28]
                          , IAmCamera 368 1234 40
                          )
                        , ( [0x81, 0x01, 0x00, 0x42]
                          , IAmDispatcher [66]
                          )
                        , ( [0x81, 0x03, 0x00, 0x42, 0x01, 0x70, 0x13, 0x88]
                          , IAmDispatcher [66, 368, 5000]
                          )
                        ]

      forM_ expectations $ \(bytes, message) ->
        it ("decodes " <> show bytes <> " to " <> show message) $ do
          runGet decodeMessage (LBS.pack bytes) `shouldBe` message

    describe "encodeMessage" $ do
      let expectations = [ ( Error "bad"
                          , [0x10, 0x03, 0x62, 0x61, 0x64]
                          )
                        , ( Error "illegal msg"
                          , [0x10, 0x0b, 0x69, 0x6c, 0x6c, 0x65, 0x67, 0x61, 0x6c, 0x20, 0x6d, 0x73, 0x67]
                          )
                        , ( TicketMsg $ Ticket "UN1X" 66 100 123456 110 123816 10000
                          , [0x21, 0x04, 0x55, 0x4e, 0x31, 0x58, 0x00, 0x42, 0x00, 0x64, 0x00, 0x01, 0xe2,
                              0x40, 0x00, 0x6e, 0x00, 0x01, 0xe3, 0xa8, 0x27, 0x10]
                          )
                        , ( TicketMsg $ Ticket "RE05BKG" 368 1234 1000000 1235 1000060 6000
                          , [0x21, 0x07, 0x52, 0x45, 0x30, 0x35, 0x42, 0x4b, 0x47, 0x01, 0x70, 0x04, 0xd2,
                              0x00, 0x0f, 0x42, 0x40, 0x04, 0xd3, 0x00, 0x0f, 0x42, 0x7c, 0x17, 0x70]
                          )
                        , ( HeartbeatMsg, [0x41] )
                        ]

      forM_ expectations $ \(message, bytes) ->
        it ("encodes " <> show message <> " to " <> show bytes) $ do
          runPut (encodeMessage message) `shouldBe` LBS.pack bytes

  describe "SpeedDaemon.Network.ClientGateway" $ do
    describe "receiveMessage" $ do
      it "receives message all at once" $ example $ do
        (serverSocket, clientSocket) <- socketPair AF_UNIX Stream defaultProtocol
        _ <- forkIO $ do
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x40, 0x00, 0x00, 0x00, 0x0a]
        msg <- receiveMessage serverSocket
        msg `shouldBe` Right (WantHeartbeat 10)

      it "receives message in multiple chunks" $ example $ do
        (serverSocket, clientSocket) <- socketPair AF_UNIX Stream defaultProtocol
        _ <- forkIO $ do
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x40, 0x00, 0x00]
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x00, 0x0a]
        msg <- receiveMessage serverSocket
        msg `shouldBe` Right (WantHeartbeat 10)

      it "receives multiple messages that are joined together" $ example $ do
        (serverSocket, clientSocket) <- socketPair AF_UNIX Stream defaultProtocol
        _ <- forkIO $ do
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x40, 0x00, 0x00]
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x00, 0x0a, 0x81, 0x01]
          send clientSocket $ (LBS.toStrict . LBS.pack) [0x00, 0x42]
        msg1 <- receiveMessage serverSocket
        msg1 `shouldBe` Right (WantHeartbeat 10)
        msg2 <- receiveMessage serverSocket
        msg2 `shouldBe` Right (IAmDispatcher [66])