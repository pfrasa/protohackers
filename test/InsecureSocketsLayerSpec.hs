module InsecureSocketsLayerSpec (spec) where

import Test.Hspec
import Control.Concurrent (forkIO)
import Control.Monad (forM_, void)
import Data.Binary.Get (runGet)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.Text ()
import Data.Text.Encoding (encodeUtf8)
import Network.Socket (socketPair, defaultProtocol, SocketType(..), Family(..))
import Network.Socket.ByteString (send)
import qualified Streaming.Prelude as S
import InsecureSocketsLayer.Cipher
import InsecureSocketsLayer.Session

spec :: Spec
spec = do
  describe "InsecureSocketsLayer.Cipher" $ do
    describe "decodeCipherSpec" $ do
      let expectations = [ ( [0x02, 0x01, 0x01, 0x00]
                           , CipherSpec [Xor 1, ReverseBits]
                           ),
                           ( [0x05, 0x05, 0x00]
                           , CipherSpec [AddPos, AddPos]
                           ),
                           ( [0x02, 0x7b, 0x05, 0x01, 0x00]
                           , CipherSpec [Xor 123, AddPos, ReverseBits]
                           )
                         ]

      forM_ expectations $ \(bytes, message) ->
        it ("decodes " <> show bytes <> " to " <> show message) $ do
          runGet decodeCipherSpec (LBS.pack bytes) `shouldBe` message

    describe "encryptBytes" $ do
      it "encrypts 'hello' correctly (example cipher 1)" $ do
        let cipher = CipherSpec [Xor 1, ReverseBits]
        let message = [0x68, 0x65, 0x6c, 0x6c, 0x6f]

        encryptBytes cipher 0 message `shouldBe` [0x96, 0x26, 0xb6, 0xb6, 0x76]

      it "encrypts 'hello' correctly (example cipher 2)" $ do
        let cipher = CipherSpec [AddPos, AddPos]
        let message = [0x68, 0x65, 0x6c, 0x6c, 0x6f]

        encryptBytes cipher 0 message `shouldBe` [0x68, 0x67, 0x70, 0x72, 0x77]

      it "encrypts sequence of messages correctly" $ do
        let cipher = CipherSpec [Xor 123, AddPos, ReverseBits]
        let clientMessage1 = BS.unpack $ encodeUtf8 "4x dog,5x car\n"
        let clientEncrypted1 = [0xf2, 0x20, 0xba, 0x44, 0x18, 0x84, 0xba, 0xaa, 0xd0, 0x26, 0x44, 0xa4, 0xa8, 0x7e]
        let clientMessage2 = BS.unpack $ encodeUtf8 "3x rat,2x cat\n"
        let clientEncrypted2 = [0x6a, 0x48, 0xd6, 0x58, 0x34, 0x44, 0xd6, 0x7a, 0x98, 0x4e, 0x0c, 0xcc, 0x94, 0x31]
        let serverMessage1 = BS.unpack $ encodeUtf8 "5x car\n"
        let serverEncrypted1 = [0x72, 0x20, 0xba, 0xd8, 0x78, 0x70, 0xee]
        let serverMessage2 = BS.unpack $ encodeUtf8 "3x rat\n"
        let serverEncrypted2 = [0xf2, 0xd0, 0x26, 0xc8, 0xa4, 0xd8, 0x7e]

        encryptBytes cipher 0 clientMessage1 `shouldBe` clientEncrypted1
        encryptBytes cipher (length clientMessage1) clientMessage2 `shouldBe` clientEncrypted2
        encryptBytes cipher 0 serverMessage1 `shouldBe` serverEncrypted1
        encryptBytes cipher (length serverMessage1) serverMessage2 `shouldBe` serverEncrypted2

    describe "decryptBytes" $ do
      it "decrypts 'hello' correctly (example cipher 1)" $ do
        let cipher = CipherSpec [Xor 1, ReverseBits]
        let message = [0x68, 0x65, 0x6c, 0x6c, 0x6f]

        decryptBytes cipher 0 [0x96, 0x26, 0xb6, 0xb6, 0x76] `shouldBe` message

      it "decrypts 'hello' correctly (example cipher 2)" $ do
        let cipher = CipherSpec [AddPos, AddPos]
        let message = [0x68, 0x65, 0x6c, 0x6c, 0x6f]

        decryptBytes cipher 0 [0x68, 0x67, 0x70, 0x72, 0x77] `shouldBe` message

      it "decrypts sequence of messages correctly" $ do
        let cipher = CipherSpec [Xor 123, AddPos, ReverseBits]
        let clientMessage1 = BS.unpack $ encodeUtf8 "4x dog,5x car\n"
        let clientEncrypted1 = [0xf2, 0x20, 0xba, 0x44, 0x18, 0x84, 0xba, 0xaa, 0xd0, 0x26, 0x44, 0xa4, 0xa8, 0x7e]
        let clientMessage2 = BS.unpack $ encodeUtf8 "3x rat,2x cat\n"
        let clientEncrypted2 = [0x6a, 0x48, 0xd6, 0x58, 0x34, 0x44, 0xd6, 0x7a, 0x98, 0x4e, 0x0c, 0xcc, 0x94, 0x31]
        let serverMessage1 = BS.unpack $ encodeUtf8 "5x car\n"
        let serverEncrypted1 = [0x72, 0x20, 0xba, 0xd8, 0x78, 0x70, 0xee]
        let serverMessage2 = BS.unpack $ encodeUtf8 "3x rat\n"
        let serverEncrypted2 = [0xf2, 0xd0, 0x26, 0xc8, 0xa4, 0xd8, 0x7e]

        decryptBytes cipher 0 clientEncrypted1 `shouldBe` clientMessage1
        decryptBytes cipher (length clientEncrypted1) clientEncrypted2 `shouldBe` clientMessage2
        decryptBytes cipher 0 serverEncrypted1 `shouldBe` serverMessage1
        decryptBytes cipher (length serverEncrypted1) serverEncrypted2 `shouldBe` serverMessage2

    describe "isInvalidCipher" $ do
      it "identifies invalid ciphers" $ do
        isInvalidCipher (CipherSpec []) `shouldBe` True
        isInvalidCipher (CipherSpec [Xor 0]) `shouldBe` True
        isInvalidCipher (CipherSpec [Xor 1, Xor 1]) `shouldBe` True
        isInvalidCipher (CipherSpec [Xor 2, Xor 2]) `shouldBe` True
        isInvalidCipher (CipherSpec [Xor 0x14, Xor 0x14]) `shouldBe` True
        isInvalidCipher (CipherSpec [ReverseBits, ReverseBits]) `shouldBe` True
        isInvalidCipher (CipherSpec [Xor 0xa0, Xor 0x0b, Xor 0xab]) `shouldBe` True

      it "doesn't pick out valid ciphers" $ do
        isInvalidCipher (CipherSpec [Xor 123, AddPos, ReverseBits]) `shouldBe` False
        isInvalidCipher (CipherSpec [Xor 1, ReverseBits]) `shouldBe` False
        isInvalidCipher (CipherSpec [AddPos, AddPos]) `shouldBe` False

  describe "InsecureSocketsLayer.Session" $ do
    describe "decryptLines" $ do
      it "can lazily decrypt lines received" $ do
        (serverSocket, clientSocket) <- socketPair AF_UNIX Stream defaultProtocol

        void $ forkIO $ do
          void $ send clientSocket $ BS.pack [0xf2, 0x20, 0xba, 0x44, 0x18]
          void $ send clientSocket $ BS.pack [0x84, 0xba, 0xaa, 0xd0, 0x26]
          void $ send clientSocket $ BS.pack [0x44, 0xa4, 0xa8, 0x7e, 0x6a, 0x48]
          void $ send clientSocket $ BS.pack [0xd6, 0x58, 0x34, 0x44, 0xd6, 0x7a, 0x98, 0x4e, 0x0c, 0xcc, 0x94, 0x31]

        let decLines = decryptedLines (CipherSpec [Xor 123, AddPos, ReverseBits]) serverSocket
        Just (line1, nextLines) <- S.uncons decLines
        Just (line2, _) <- S.uncons nextLines

        line1 `shouldBe` "4x dog,5x car"
        line2 `shouldBe` "3x rat,2x cat"

    describe "findToyToCopy" $ do
      let expected = [ ("10x toy car,15x dog on a string,4x inflatable motorcycle", "15x dog on a string")
                     , ("4x dog,5x car", "5x car")
                     , ("3x rat,2x cat", "3x rat")
                     ]

      forM_ expected $ \(input, output) ->
        it ("returns " <> show output <> " when given " <> show input) $ do
          findToyToCopy input `shouldBe` output