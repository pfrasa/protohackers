module Main (main) where

import Data.Text (Text)
import Data.Word
import qualified Data.Text as T
import Control.Concurrent
import Control.Monad
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Network.Simple.TCP
import Data.Text.Encoding

main :: IO ()
main = connect "localhost" "8000" $ \(socket, _) -> do
  listenToSocket socket
  forkIO $ collectUserInput socket
  forever $ listenToSocket socket

  where
    collectUserInput :: Socket -> IO ()
    collectUserInput socket = forever $ do
      msg <- T.pack <$> getLine
      send socket $ encodeUtf8 $ msg <> "\n"

    listenToSocket :: Socket -> IO ()
    listenToSocket socket = do
      mMsg <- recvFully socket
      case mMsg of
           Nothing -> return ()
           Just msg ->
             putStrLn $ T.unpack msg

recvFully :: Socket -> IO (Maybe Text)
recvFully socket = do
  received <- go ""
  return $ if received == "" || lastChar received /= 10
     then Nothing
     else Just $ parse $ stripLastChar received

  where
    lastChar :: ByteString -> Word8
    lastChar = BS.head . BS.reverse

    stripLastChar :: ByteString -> ByteString
    stripLastChar = BS.reverse . BS.tail . BS.reverse

    parse :: ByteString -> Text
    parse = decodeASCII

    go :: ByteString -> IO ByteString
    go acc = do
      input <- recv socket 1000000
      case input of
           Nothing -> return acc
           Just input' ->
             let newAcc = acc <> input'
             in  if lastChar input' == 10
                    then return newAcc
                    else go newAcc
