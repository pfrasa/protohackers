module Main (main) where

import System.Environment
import Network.Simple.TCP

main :: IO ()
main = do
  args <- getArgs
  let port = args !! 0
  serve (Host "0.0.0.0") port accept
  where
    accept :: (Socket, SockAddr) -> IO ()
    accept (socket, addr) = do
      input <- recv socket 1000000
      case input of
           Nothing -> closeSock socket
           Just input' -> send socket input' >> accept (socket, addr)
