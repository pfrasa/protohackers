module Main (main) where

import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding
import Data.Map (Map)
import qualified Data.Map as M
import Network.Socket
import System.Environment
import Network.Socket.ByteString
import Control.Monad
import Data.Maybe

type Key = Text
type Value = Text

data Request
  = Insert Key Value
  | Retrieve Key
  | Version

newtype DB = DB (Map Key Value)

newDB :: DB
newDB = DB M.empty

insert :: Key -> Value -> DB -> DB
insert key value (DB m) = DB $ M.insert key value m

retrieve :: Key -> DB -> Maybe Value
retrieve key (DB m) = M.lookup key m

parseRequest :: ByteString -> Maybe Request
parseRequest input = rightToMaybe (decodeUtf8' input) >>= \string ->
  let (key, eqVal) = T.breakOn "=" string
  in  if key == "version" then
        if eqVal == "" then Just Version else Nothing
      else if eqVal == "" then
        Just (Retrieve key)
      else
        let val = T.drop 1 eqVal
        in  Just (Insert key val)

rightToMaybe :: Either a b -> Maybe b
rightToMaybe = either (const Nothing) Just

handleRequest :: Request -> DB -> (DB, Maybe Text)
handleRequest request db =
  case request of
    Insert key value -> (insert key value db, Nothing)
    Retrieve key -> 
      let value = fromMaybe "" $ retrieve key db
      in  (db, Just (key <> "=" <> value))
    Version -> (db, Just versionString)
  where
    versionString = "version=My Key-Value Store"

main :: IO ()
main = do
  args <- getArgs
  let port = read (head args) :: PortNumber

  socket <- openSocket port
  loop newDB socket
  where
    openSocket :: PortNumber -> IO Socket
    openSocket port = do
      sock <- socket AF_INET Datagram defaultProtocol 
      bind sock (SockAddrInet port (tupleToHostAddress (0, 0, 0, 0)))
      return sock

    loop :: DB -> Socket -> IO ()
    loop db sock = do
      (bytes, clientAddr) <- recvFrom sock 1000
      print bytes
      let mReq = parseRequest bytes
      case mReq of
        Just req -> do
          let (newDB, mResponse) = handleRequest req db
          forM_ mResponse $ \response -> sendTo sock (encodeUtf8 response) clientAddr
          loop newDB sock
        Nothing ->
          loop db sock

