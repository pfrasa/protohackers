{-# LANGUAGE DeriveGeneric #-}

module Main (main) where

import GHC.Generics
import System.Environment
import Network.Simple.TCP
import Data.ByteString hiding (all, putStr, filter)
import qualified Data.ByteString as BS
import Data.Aeson
import Data.Aeson.Extra

main :: IO ()
main = do
  args <- getArgs
  let port = args !! 0
  serve (Host "0.0.0.0") port accept
  where
    accept :: (Socket, SockAddr) -> IO ()
    accept (socket, addr) = do
      input <- recvFully
      case input of
           Nothing -> abort
           Just lines -> do
             putStr "Received: "
             print lines
             allValid <- handle (responsesFor lines)
             if allValid then continue else return ()
      where
        recvFully :: IO (Maybe ByteString)
        recvFully = do
          received <- go ""
          return $ if received == ""
             then Nothing
             else Just received
          where
            go :: ByteString -> IO ByteString
            go acc = do
              input <- recv socket 1000000
              case input of
                   Nothing -> return acc
                   Just input' ->
                     let lastChar = BS.head $ BS.reverse input'
                         newAcc = acc <> input'
                     in  if lastChar == 10
                            then return newAcc
                            else go newAcc

        continue :: IO ()
        continue = accept (socket, addr)

        abort :: IO ()
        abort = send socket "}{" >> closeSock socket

        handle :: [Maybe ByteString] -> IO Bool
        handle [] = return True
        handle ((Just response):rs) = do
          putStr "Sending: "
          print response
          send socket response
          handle rs
        handle (Nothing:_) = abort >> return False

data Request = Request { request_method :: String, number :: Double } deriving Generic

instance FromJSON Request where
  parseJSON = genericParseJSON $ defaultOptions { fieldLabelModifier = handleMethod }
    where
      handleMethod "request_method" = "method"
      handleMethod s = s

data Response = Response { method :: String, prime :: Bool } deriving Generic

instance ToJSON Response where

getValidRequest :: ByteString -> Maybe Request
getValidRequest input = do
  request <- decodeStrict input
  if request_method request == "isPrime"
     then Just request
     else Nothing

responsesFor :: ByteString -> [Maybe ByteString]
responsesFor lines = responseFor <$> splitLines lines
  where
    splitLines :: ByteString -> [ByteString]
    splitLines = filter (/= "") . split 10

    responseFor :: ByteString -> Maybe ByteString
    responseFor line = responseFor' <$> getValidRequest line

    responseFor' :: Request -> ByteString
    responseFor' request =
      let response = Response "isPrime" (isPrime (number request))
      in  (encodeStrict response) <> "\n"

isPrime :: Double -> Bool
isPrime double =
  let num = round double
      isInt = double == fromInteger num
      upperBound = ceiling $ sqrt double
  in  isInt && num > 1 && all (not . (strictlyDivides num)) [2..upperBound]
  where
    strictlyDivides :: Integer -> Integer -> Bool
    strictlyDivides prod factor = prod /= factor && prod `rem` factor == 0
