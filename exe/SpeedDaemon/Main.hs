module Main (main) where

import Network.Simple.TCP (serve, HostPreference (Host))
import System.Environment (getArgs)
import SpeedDaemon.Core.Queues (mkQueues)
import SpeedDaemon.Actor.MessageDispatcher (startMessageDispatch)
import SpeedDaemon.Actor.TicketHandler (startTicketHandling)
import SpeedDaemon.Actor.MeasurementHandler (startMeasurementHandling)
import SpeedDaemon.ClientHandler (mkClientState, handleClient)

main :: IO ()
main = do
  port : _ <- getArgs

  queues <- mkQueues
  _ <- startMessageDispatch queues
  _ <- startTicketHandling queues
  _ <- startMeasurementHandling queues

  serve (Host "0.0.0.0") port $ \(sock, _) -> do
    let state = mkClientState queues sock
    handleClient state
