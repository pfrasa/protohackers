module Main (main) where

import Control.Monad (forever)
import qualified Data.Text as T
import Network.Socket (tupleToHostAddress)
import System.Environment (getArgs)
import qualified LineReversal.LRCP as LRCP

main :: IO ()
main = do
  port : _ <- getArgs

  LRCP.accept (tupleToHostAddress (0, 0, 0, 0)) (read port) $ \session -> forever $ do
    message <- LRCP.receive session
    LRCP.send session (T.reverse message)