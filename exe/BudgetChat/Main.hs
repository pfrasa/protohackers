module Main (main) where

import System.Environment
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Char
import Data.Text (Text)
import qualified Data.Text as T
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Word
import Data.Text.Encoding
import Control.Concurrent
import Network.Simple.TCP
import Data.Coerce

newtype User = User Text deriving (Eq, Ord)

type ChatState = MVar (Set User)

removeUser :: User -> ChatState -> IO ()
removeUser user = onState (Set.delete user)

onState :: (Set User -> Set User) -> ChatState -> IO ()
onState f state = do
  users <- takeMVar state
  putMVar state $ f users

type EventQueue = Chan Event

data Event
  = Connected User
  | Disconnected User
  | Message User Text


main :: IO ()
main = do
  port : _ <- getArgs

  state <- newMVar Set.empty
  queue <- newChan

  serve (Host "0.0.0.0") port $ \(socket, _) -> userWorker socket state queue

userWorker :: Socket -> ChatState -> EventQueue -> IO ()
userWorker socket chatState origQueue = do
  send socket "Welcome to the chat! What shall I call you?\n"
  mUserName <- recvFully socket
  case mUserName of
       Nothing -> do
         send socket "Shutting down\n"
         closeSock socket
       Just userName ->
         if not (validName userName)
           then do
             send socket "Invalid username! Shutting down\n"
             closeSock socket
           else do
             let user = User userName

             users <- takeMVar chatState
             if Set.member user users
               then do
                 putMVar chatState users

                 send socket "Username already taken! Shutting down\n"
                 closeSock socket
               else do
                 putMVar chatState $ Set.insert user users

                 send socket $ encodeUtf8 $ userListMsg users

                 queue <- dupChan origQueue
                 writeChan queue (Connected user)

                 eventHandlerId <- forkIO $ relayEventsToUser user queue
                 listenToUserSocket user queue eventHandlerId

  where
    validName :: Text -> Bool
    validName = T.all isAlphaNum

    userListMsg :: Set User -> Text
    userListMsg users =
      "* The room contains: " <> T.intercalate ", " (coerce (Set.elems users)) <> "\n"

    listenToUserSocket :: User -> EventQueue -> ThreadId -> IO ()
    listenToUserSocket user queue eventHandlerId = loop
      where
        loop :: IO ()
        loop = do
          mMsg <- recvFully socket
          case mMsg of
               Nothing -> do
                 send socket "Bye, bye!\n"
                 killThread eventHandlerId
                 closeSock socket

                 removeUser user chatState

                 writeChan queue $ Disconnected user

               Just msg -> do
                 writeChan queue $ Message user msg
                 loop

    relayEventsToUser :: User -> EventQueue -> IO ()
    relayEventsToUser thisUser queue = loop
      where
        loop :: IO ()
        loop = do
          event <- readChan queue
          case event of
               Connected otherUser -> broadcast otherUser joinMsg
               Disconnected otherUser -> broadcast otherUser leaveMsg
               Message otherUser msg -> broadcast otherUser (chatMsg msg)
          loop

        broadcast :: User -> (User -> Text) -> IO ()
        broadcast otherUser mkMessage
          | thisUser /= otherUser = send socket $ encodeUtf8 $ mkMessage otherUser
          | otherwise = return ()

        joinMsg :: User -> Text
        joinMsg user = "* " <> (coerce user) <> " has entered the room" <> "\n"

        leaveMsg :: User -> Text
        leaveMsg user = "* " <> (coerce user) <> " has left the room" <> "\n"

        chatMsg :: Text -> User -> Text
        chatMsg msg user = "[" <> (coerce user) <> "] " <> msg <> "\n"

recvFully :: Socket -> IO (Maybe Text)
recvFully socket = do
  received <- go ""
  return $ if received == "" || lastChar received /= 10
     then Nothing
     else Just $ parse $ stripLastChar received

  where
    lastChar :: ByteString -> Word8
    lastChar = BS.head . BS.reverse

    stripLastChar :: ByteString -> ByteString
    stripLastChar = BS.reverse . BS.tail . BS.reverse

    parse :: ByteString -> Text
    parse = decodeASCII

    go :: ByteString -> IO ByteString
    go acc = do
      input <- recv socket 1000000
      case input of
           Nothing -> return acc
           Just input' ->
             let newAcc = acc <> input'
             in  if lastChar input' == 10
                    then return newAcc
                    else go newAcc
