module Main (main) where

import Data.Word
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Network.Simple.TCP
import System.Environment
import RBST (RBST, rbstTree)
import qualified RBST
import Data.Binary.Get
import Data.Binary.Put
import Data.Int

main :: IO ()
main = do
  args <- getArgs
  let port = head args
  serve (Host "0.0.0.0") port $ accept emptyStore

  where
    accept :: Store -> (Socket, SockAddr) -> IO ()
    accept store (socket, addr) = do
      input <- recvUntil (\bs -> BS.length bs >= 9) socket
      putStr "Received (raw): "
      print input
      case input >>= parseRequest of
           Nothing -> closeSock socket
           Just request -> do
             putStr "Received (decoded): "
             print request
             let (newStore, response) = handleRequest store request
             sendResponse socket response
             accept newStore (socket, addr)

    recvUntil :: (ByteString -> Bool) -> Socket -> IO (Maybe ByteString)
    recvUntil pred socket = do
      received <- go ""
      return $ if received == ""
                  then Nothing
                  else Just received
      where
        go :: ByteString -> IO ByteString
        go acc = do
          input <- recv socket 9
          case input of
               Nothing -> return acc
               Just input' ->
                 let newAcc = acc <> input'
                 in  if pred newAcc
                        then return newAcc
                        else go newAcc

    sendResponse :: Socket -> Maybe Response -> IO ()
    sendResponse _ Nothing = return ()
    sendResponse socket (Just (Response mean)) = do
      putStr "Reponse (unencoded): "
      print mean
      let encoded = (LBS.toStrict . runPut . putInt32be . fromIntegral) mean
      putStr "Response (encoded): "
      print encoded
      send socket encoded

type Timestamp = Int
type MinTime = Int
type MaxTime = Int
type Price = Int

data Request
  = Insert Timestamp Price
  | Query MinTime MaxTime
  deriving Show

newtype Response = Response Int deriving Show

newtype Store = Store (RBST Timestamp Price) deriving Show

emptyStore :: Store
emptyStore = Store RBST.empty

insert :: Store -> Timestamp -> Price -> Store
insert (Store rbst) timestamp price = Store $ RBST.insert timestamp price rbst

find :: Store -> MinTime -> MaxTime -> [Price]
find (Store rbst) minTime maxTime = find' (rbstTree rbst)
  where
    find' :: RBST.Tree Timestamp Price -> [Price]
    find' tree = case tree of
       RBST.Empty -> []
       RBST.Node _ timestamp left price right ->
         let thisResult = [price | timestamp >= minTime && timestamp <= maxTime]
             leftResult = if timestamp >= minTime then find' left else []
             rightResult = if timestamp <= maxTime then find' right else []
         in  leftResult ++ thisResult ++ rightResult

handleRequest :: Store -> Request -> (Store, Maybe Response)
handleRequest store request =
  case request of
       Insert timestamp price ->
         let newStore = insert store timestamp price
         in  (newStore, Nothing)
       Query minTime maxTime ->
         let results = find store minTime maxTime
             avg = round $ fromIntegral (sum results) / fromIntegral (length results)
         in  (store, Just (Response avg))

parseRequest :: ByteString -> Maybe Request
parseRequest input = do
  (typePart, arg1, arg2) <- runGetMaybe getData input
  constructor <- parseType typePart
  return $ constructor (fromIntegral arg1) (fromIntegral arg2)

  where
    getData :: Get (Word8, Int32, Int32)
    getData = do
      typePart <- getWord8
      arg1 <- getWord32be
      arg2 <- getWord32be
      return (typePart, fromIntegral arg1, fromIntegral arg2)

    parseType :: Word8 -> Maybe (Int -> Int -> Request)
    parseType 73 = Just Insert
    parseType 81 = Just Query
    parseType _ = Nothing

runGetMaybe :: Get a -> ByteString -> Maybe a
runGetMaybe getter bs = do
  (_, _, x) <- rightToMaybe $ runGetOrFail getter (LBS.fromStrict bs)
  return x

rightToMaybe :: Either a b -> Maybe b
rightToMaybe = either (const Nothing) Just