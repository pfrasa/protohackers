module Main (main) where

import Network.Simple.TCP
import System.Environment
import InsecureSocketsLayer.Session (handleClient)

main :: IO ()
main = do
  port : _ <- getArgs

  serve (Host "0.0.0.0") port $ \(socket, _) -> handleClient socket