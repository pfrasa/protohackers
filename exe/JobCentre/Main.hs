module Main (main) where

import Control.Concurrent
import Control.Exception (SomeException, catch)
import Control.Monad (void, when, forever)
import Network.Simple.TCP
import System.Environment (getArgs)

import JobCentre.API
import JobCentre.Common
import JobCentre.JobServer
import JobCentre.Network

main :: IO ()
main = do
  port : _ <- getArgs

  jobServer@(JobServer mvar) <- mkJobServer
  inQueue <- newChan

  void $ forkIO $ handle inQueue jobServer

  serve (Host "0.0.0.0") port $ \(sock, _) -> do
    clientId <- mkClientId
    void $ modifyMVar_ mvar (return . addClient clientId)
    accept_ sock jobServer clientId inQueue

  where
    accept_ :: Socket -> JobServer -> ClientId -> Chan (Request, Socket, ClientId) -> IO ()
    accept_ sock jobServer clientId inQueue = do
      request <- recvRequest sock `catch` \(_ :: SomeException) -> return Disconnect
      putStrLn $ "[Request] (" ++ show clientId ++ ") " ++ show request

      case request of
        Disconnect -> void $ handleRequest jobServer request clientId
        _ -> do
          writeChan inQueue (request, sock, clientId)
          accept_ sock jobServer clientId inQueue

    handle :: Chan (Request, Socket, ClientId) -> JobServer -> IO () 
    handle inQueue jobServer@(JobServer mvar) = forever $ do
      (request, sock, clientId) <- readChan inQueue
      result <- handleRequest jobServer request clientId

      case result of
        Direct response -> do
          putStrLn $ "[Response] (" ++ show clientId ++ ") " ++ show response
          sendResponse sock response `catch` \(_ :: SomeException) -> return ()
        TryAgain -> do
          state <- readMVar mvar
          when (containsClient state clientId) $ writeChan inQueue (request, sock, clientId)
        Empty -> return ()