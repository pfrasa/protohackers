module Main (main) where

import System.Environment
import Data.Text (Text)
import qualified Data.Text as T
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Word
import Data.Text.Encoding
import Network.Simple.TCP
import Control.Concurrent
import Text.RegexPR

main :: IO ()
main = do
  port : _ <- getArgs

  serve (Host "0.0.0.0") port $ \(socket, _) -> runWith socket

type ConnectionVar = MVar Bool

runWith :: Socket -> IO ()
runWith userProxySocket = do
  connect "chat.protohackers.com" "16963" $ \(proxyServerSocket, _) -> do
    connectionVar <- newMVar True
    forkIO $ proxyMessages userProxySocket proxyServerSocket connectionVar
    proxyMessages proxyServerSocket userProxySocket connectionVar

proxyMessages :: Socket -> Socket -> ConnectionVar -> IO ()
proxyMessages receiveEnd writeEnd connectionVar = loop
  where
    loop :: IO ()
    loop = do
      isOpen <- readMVar connectionVar
      if isOpen then do
        mMessage <- recvFully receiveEnd
        case mMessage of
          Nothing -> do
            closeSock receiveEnd
            closeSock writeEnd
            putMVar connectionVar False
            pure ()
          Just message -> do
            let modifiedMessage = modifyMessage message
            send writeEnd $ encodeUtf8 modifiedMessage <> "\n"
            loop
      else do
        pure ()

modifyMessage :: Text -> Text
modifyMessage m =
  let addressPattern = "7\\w{25,34}"
      regex = "(^|(?<=\\s))" <> addressPattern <> "((?=\\s)|$)"
  in  T.pack $ gsubRegexPR regex "7YWHMfk9JZe0LM0g1ZauHuiSxhI" (T.unpack m)

recvFully :: Socket -> IO (Maybe Text)
recvFully socket = do
  received <- go ""
  return $ if received == "" || lastChar received /= 10
     then Nothing
     else Just $ parse $ stripLastChar received

  where
    lastChar :: ByteString -> Word8
    lastChar = BS.head . BS.reverse

    stripLastChar :: ByteString -> ByteString
    stripLastChar = BS.reverse . BS.tail . BS.reverse

    parse :: ByteString -> Text
    parse = decodeASCII

    go :: ByteString -> IO ByteString
    go acc = do
      input <- recv socket 1000000
      case input of
           Nothing -> return acc
           Just input' ->
             let newAcc = acc <> input'
             in  if lastChar input' == 10
                    then return newAcc
                    else go newAcc
